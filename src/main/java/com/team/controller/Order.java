package com.team.controller;

import com.team.cities.City;
import com.team.cities.DniproStore;
import com.team.cities.KyivStore;
import com.team.cities.LvivStore;
import com.team.model.bouquet.Bouquet;
import com.team.model.cards.*;
import com.team.model.delivery.Delivery;
import com.team.model.events.*;
import com.team.model.packaging.Package;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;

public class Order {
    private Bouquet bouquet;
    private Event event;
    private Card card;
    private Delivery delivery;
    private Package packaging;
    private List<Bouquet> bouquets = new LinkedList<>();

    public void setOccasion(City city, Occasion occasion) {
        if (city.equals(City.KYIV)) {
            this.event = new KyivStore().makeEvent(occasion);
        } else if (city.equals(City.DNIPRO)) {
            this.event = new DniproStore().makeEvent(occasion);
        } else if (city.equals(City.LVIV)) {
            this.event = new LvivStore().makeEvent(occasion);
        }
    }

    public void setCard(CardType card) {
        if (card.equals(CardType.BONUS)) {
            this.card = new BonusCard();
        } else if (card.equals(CardType.GOLD)) {
            this.card = new GoldCard();
        } else if (card.equals(CardType.SOCIAL)) {
            this.card = new SocialCard();
        }
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public void setPackaging(Package packaging) {
        this.packaging = packaging;
    }

    public void setBucket(Bouquet bouquet) {
        this.bouquet = bouquet;
        bouquets.add(bouquet);
    }

    public double getPrice() {
        double cost = getBouquets().stream().mapToDouble(Bouquet::getCost).sum();
        return (cost + delivery.getPrice() * (!card.isFreeDelivery() ? 1 : 0)
                + packaging.getPrice() * (!card.isFreePackaging() ? 1 : 0)
                - card.getDiscount()) * event.getDiscount();
    }

    public List<Bouquet> getBouquets() {
        return bouquets;
    }

    public String getInfo() {
        DecimalFormat precision = new DecimalFormat("#0.00");
        double finalPrice = getPrice();
        String inf = precision.format(finalPrice);
        bouquets.forEach(System.out::println);
        return "\n" + card.toString() + "\n"
                + delivery.toString() + "\n"
                + packaging.toString() + "\n"
                + "Final price: " + inf + " uah";
    }
}
