package com.team.cities;

import com.team.model.events.Event;
import com.team.model.events.Occasion;

public interface Store {
    Event chooseEvent(Occasion occasion);

    Event makeEvent(Occasion occasion);
}
