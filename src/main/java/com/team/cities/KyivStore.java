package com.team.cities;

import com.team.model.events.*;

public class KyivStore implements Store {

    @Override
    public final Event chooseEvent(final Occasion occasion) {
        if (occasion.equals(Occasion.VALENTINE)) {
            return new ValentinesDay(1.2);
        } else if (occasion.equals(Occasion.BIRTHDAY)) {
            return new Birthday(0.95);
        } else if (occasion.equals(Occasion.FUNERAL)) {
            return new Funeral(0.9);
        } else if (occasion.equals(Occasion.WEDDING)) {
            return new Wedding(0.98);
        }else if(occasion.equals(Occasion.PERSONAL)){
            return new Personal(1.0);
        }
        return null;
    }

    @Override
    public final Event makeEvent(final Occasion occasion) {
        Event event = chooseEvent(occasion);
        assert event != null;
        return event;
    }

    @Override
    public String toString() {
        return "Store: Kyiv";
    }
}
