package com.team.model.packaging;

public enum Package {
    PAPER(20), NET(15),
    RIBBON(10), ORGANZA(25), WITHOUT_PACKAGING(0);

    private final double price;

    Package(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Package price: " + price + " uah";
    }
}
