package com.team.model.bouquet.additionalcomponents;

import com.team.model.bouquet.BouquetDecorator;

public class PionuFlower extends BouquetDecorator {

    public PionuFlower(int amount) {
        String name = "Pionu";
        super.setAdditionalFlowers(amount + " " + name);
        double price = 20;
        super.setAdditionalPrice(price * amount);
    }
}
