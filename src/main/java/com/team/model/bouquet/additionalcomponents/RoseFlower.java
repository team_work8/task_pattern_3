package com.team.model.bouquet.additionalcomponents;

import com.team.model.bouquet.BouquetDecorator;

public class RoseFlower extends BouquetDecorator {

    public RoseFlower(int amount) {
        String name = "Rose";
        super.setAdditionalFlowers(amount + " " + name);
        double price = 40;
        super.setAdditionalPrice(price * amount);
    }

}
