package com.team.model.bouquet.additionalcomponents;

import com.team.model.bouquet.BouquetDecorator;

public class TulipFlower extends BouquetDecorator {

    public TulipFlower(int amount) {
        String name = "Tulip";
        super.setAdditionalFlowers(amount + " " + name);
        double price = 40;
        super.setAdditionalPrice(price * amount);
    }
}
