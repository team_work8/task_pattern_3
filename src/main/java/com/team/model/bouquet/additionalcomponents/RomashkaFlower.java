package com.team.model.bouquet.additionalcomponents;

import com.team.model.bouquet.BouquetDecorator;

public class RomashkaFlower extends BouquetDecorator {

    public RomashkaFlower(int amount) {
        String name = "Romashka";
        super.setAdditionalFlowers(amount + " " + name);
        double price = 30;
        super.setAdditionalPrice(price * amount);
    }
}
