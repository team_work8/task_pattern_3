package com.team.model.bouquet;

import java.util.List;

public interface Bouquet {

    double getCost();

    String getName();

    List<String> getFlowers();
}
