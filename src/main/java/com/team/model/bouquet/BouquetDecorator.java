package com.team.model.bouquet;

import java.util.List;

public class BouquetDecorator implements Bouquet {

    private Bouquet bouquet;
    private double additionalPrice = 0;
    private String additionalFlowers = "";

    public void setFlowers(Bouquet newFlower) {
        bouquet = newFlower;
        if (additionalFlowers != null) {
            bouquet.getFlowers().add(additionalFlowers);
        }
    }

    public void setAdditionalPrice(double additionalPrice) {
        this.additionalPrice = additionalPrice;
    }

    public void setAdditionalFlowers(String additionalFlowers) {
        this.additionalFlowers = additionalFlowers;
    }

    @Override
    public double getCost() {
        return bouquet.getCost() + additionalPrice;
    }

    @Override
    public String getName() {
        return bouquet.getName() + bouquet.getFlowers();
    }


    @Override
    public List<String> getFlowers() {
        return bouquet.getFlowers();
    }

    @Override
    public String toString() {
        return "Decorated bouquet: "
                + bouquet
                + ", and additionalPrice for added flowers: " + additionalPrice + " uah";
    }
}
