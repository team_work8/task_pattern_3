package com.team.model.delivery;

public enum Delivery {

    WITHIN_CITY(50), WITHOUT_DELIVERY(0), HOLIDAY(100);

    private final double price;

    Delivery(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Delivery price: " + price + " uah";
    }
}
