package com.team.model.cards;

public class SocialCard implements Card {
    private double discount = 0;

    @Override
    public double getDiscount() {
        return discount;
    }

    @Override
    public boolean isFreePackaging() {
        return true;
    }

    @Override
    public boolean isFreeDelivery() {
        return true;
    }

    @Override
    public String toString() {
        return "Social card with " +
                discount + " uah discount, " +
                "with free packaging, " +
                "with free delivery";
    }
}
