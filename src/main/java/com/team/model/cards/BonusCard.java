package com.team.model.cards;

public class BonusCard implements Card {
    private double discount = 20;

    @Override
    public double getDiscount() {
        return discount;
    }

    @Override
    public boolean isFreePackaging() {
        return false;
    }

    @Override
    public boolean isFreeDelivery() {
        return false;
    }

    @Override
    public String toString() {
        return "Bonus card with " +
                discount + " uah discount, " +
                "without free packaging, " +
                "with free delivery";
    }
}
