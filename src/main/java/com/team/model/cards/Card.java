package com.team.model.cards;

public interface Card {

    double getDiscount();

    boolean isFreePackaging();

    boolean isFreeDelivery();
}
