package com.team.model.cards;

public enum CardType {
    GOLD,
    SOCIAL,
    BONUS
}
