package com.team.model.cards;

public class GoldCard implements Card {
    private double discount = 50;

    @Override
    public double getDiscount() {
        return discount;
    }

    @Override
    public boolean isFreePackaging() {
        return false;
    }

    @Override
    public boolean isFreeDelivery() {
        return true;
    }

    @Override
    public String toString() {
        return "Gold card with " +
                discount + " uah discount, " +
                "without free packaging, " +
                "with free delivery";
    }
}
