package com.team.model.events;

public class Birthday implements Event {

    private double discount;

    public Birthday(double discount) {
        this.discount = discount;
    }

    @Override
    public String info() {
        return "For Birthday";
    }

    @Override
    public double getDiscount() {
        return discount;
    }

}
