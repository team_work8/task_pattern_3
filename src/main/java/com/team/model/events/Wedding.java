package com.team.model.events;

public class Wedding implements Event {
    private double discount;

    public Wedding(double discount) {
        this.discount = discount;
    }

    @Override
    public String info() {
        return "For Wedding";
    }

    @Override
    public double getDiscount() {
        return discount;
    }

}
