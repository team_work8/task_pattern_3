package com.team.model.events;

public class ValentinesDay implements Event {

    private double discount;

    public ValentinesDay(double discount) {
        this.discount = discount;
    }

    @Override
    public String info() {
        return "For Valentine`s Day";
    }

    @Override
    public double getDiscount() {
        return discount;
    }

}
