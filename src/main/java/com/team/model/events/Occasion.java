package com.team.model.events;

public enum Occasion {
    VALENTINE,
    BIRTHDAY,
    WEDDING,
    FUNERAL,
    PERSONAL
}
