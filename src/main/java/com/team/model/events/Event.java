package com.team.model.events;

public interface Event {

    String info();

    double getDiscount();
}
