package com.team.model.events;

public class Personal implements Event {

    private double discount;

    public Personal(double discount) {
        this.discount = discount;
    }

    @Override
    public String info() {
        return "For personal purpose";
    }

    @Override
    public double getDiscount() {
        return discount;
    }
}
