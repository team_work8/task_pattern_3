package com.team.model.events;

public class Funeral implements Event {
    private double discount;

    public Funeral(double discount) {
        this.discount = discount;
    }

    @Override
    public String info() {
        return "For Funeral ";
    }

    @Override
    public double getDiscount() {
        return discount;
    }

}
