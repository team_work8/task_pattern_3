package com.team.model.flowers;

import com.team.model.bouquet.Bouquet;

import java.util.ArrayList;
import java.util.List;

public class Pionu implements Bouquet {

    private String name = "Pionu";
    private double cost;
    private List<String> flowers;

    public Pionu(int amount) {
        flowers = new ArrayList<>();
        flowers.add(amount + " " + name);
        this.cost = 20 * amount;
    }

    public List<String> getFlowers() {
        return flowers;
    }

    @Override
    public double getCost() {
        return cost;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        String res = "Pionu bouquet with price: " + cost + " uah, flowers: " + flowers;
        return res.replaceAll("\\[", "").replaceAll("]","");
    }
}
