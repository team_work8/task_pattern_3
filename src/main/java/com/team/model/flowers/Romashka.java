package com.team.model.flowers;

import com.team.model.bouquet.Bouquet;

import java.util.ArrayList;
import java.util.List;

public class Romashka implements Bouquet {
    private String name = "Romashka";
    private double cost;
    private List<String> flowers;

    public Romashka(int amount) {
        flowers = new ArrayList<>();
        flowers.add(amount + " " + name);
        this.cost = 30 * amount;
    }

    @Override
    public double getCost() {
        return cost;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<String> getFlowers() {
        return flowers;
    }

    @Override
    public String toString() {
        String res = "Romashka bouquet with price: " + cost + " uah, flowers: " + flowers;
        return res.replaceAll("\\[", "").replaceAll("]","");
    }
}
