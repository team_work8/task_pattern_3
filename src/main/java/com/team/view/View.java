package com.team.view;

import com.team.Application;
import com.team.cities.City;
import com.team.controller.Order;
import com.team.model.bouquet.Bouquet;
import com.team.model.bouquet.BouquetDecorator;
import com.team.model.bouquet.additionalcomponents.PionuFlower;
import com.team.model.bouquet.additionalcomponents.RomashkaFlower;
import com.team.model.bouquet.additionalcomponents.RoseFlower;
import com.team.model.bouquet.additionalcomponents.TulipFlower;
import com.team.model.cards.CardType;
import com.team.model.delivery.Delivery;
import com.team.model.events.Occasion;
import com.team.model.flowers.Pionu;
import com.team.model.flowers.Romashka;
import com.team.model.flowers.Rose;
import com.team.model.flowers.Tulip;
import com.team.model.packaging.Package;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    private static Order order;
    private static City city;
    private static Occasion occasion;
    private static CardType cardType;
    private static Scanner scanner;
    private static Bouquet bouquet;
    private static BouquetDecorator bouquetDecorator;
    private static Map<Integer, String> options;
    private static Map<Integer, String> cities;
    private static Map<Integer, String> occasions;
    private static Map<Integer, String> cards;
    private static Map<Integer, String> deliveries;
    private static Map<Integer, String> packages;
    private static Map<Integer, String> flowers;
    private static Map<Integer, Executable> methods;

    public View() {
        scanner = new Scanner(System.in);
        order = new Order();
        bouquetDecorator = new BouquetDecorator();

        options = new LinkedHashMap<>();
        options.put(1, "1 - Add a new bouquet to order");
        options.put(2, "2 - Delete a bouquet from order");
        options.put(3, "3 - Show current order");
        options.put(4, "4 - Decorate a particular bouquet");
        options.put(5, "5 - Complete order");
        options.put(6, "6 - Exit");

        cities = new LinkedHashMap<>();
        cities.put(1, "1 - Lviv");
        cities.put(2, "2 - Kyiv");
        cities.put(3, "3 - Dnipro");

        occasions = new LinkedHashMap<>();
        occasions.put(1, "1 - Valentine`s Day");
        occasions.put(2, "2 - Birthday");
        occasions.put(3, "3 - Wedding");
        occasions.put(4, "4 - Funeral");
        occasions.put(5, "5 - Personal purpose");

        cards = new LinkedHashMap<>();
        cards.put(1, "1 - Gold card");
        cards.put(2, "2 - Social card");
        cards.put(3, "3 - Bonus card");

        deliveries = new LinkedHashMap<>();
        deliveries.put(1, "1 - Delivery within your city");
        deliveries.put(2, "2 - Without delivery");
        deliveries.put(3, "3 - Delivery within your city during Valentine`s Day");

        packages = new LinkedHashMap<>();
        packages.put(1, "1 - Paper packaging");
        packages.put(2, "2 - Net packaging");
        packages.put(3, "3 - Ribbon packaging");
        packages.put(4, "4 - Organza packaging");
        packages.put(5, "5 - Without packaging");

        flowers = new LinkedHashMap<>();
        flowers.put(1, "1 - Pionu");
        flowers.put(2, "2 - Romashkas");
        flowers.put(3, "3 - Roses");
        flowers.put(4, "4 - Tulips");

        methods = new LinkedHashMap<>();
        methods.put(1, this::add);
        methods.put(2, this::delete);
        methods.put(3, this::showOrder);
        methods.put(4, this::decorate);
        methods.put(5, this::completeOrder);
        methods.put(6, this::exit);

        chooseCity();
        chooseOccasion();
        chooseCard();

        order.setOccasion(city, occasion);
        order.setCard(cardType);
    }

    private void add() {
        Application.logger.trace("\n~~~ Choose flowers ~~~");
        flowers.values().forEach(System.out::println);
        int key;
        Application.logger.trace("\nYour choice");
        System.out.print("--->");
        key = scanner.nextInt();
        Application.logger.trace("Input amount ");
        System.out.print("--->");
        int amount = scanner.nextInt();
        switch (key) {
            case 1:
                bouquet = new Pionu(amount);
                break;
            case 2:
                bouquet = new Romashka(amount);
                break;
            case 3:
                bouquet = new Rose(amount);
                break;
            case 4:
                bouquet = new Tulip(amount);
                break;
            default:
                Application.logger.trace("Wrong flowers input".toUpperCase());
                break;
        }
        order.setBucket(bouquet);
        System.out.println();
    }

    private void delete() {
        Application.logger.trace("Input number of the bouquet in order of addition\n");
        int key;
        System.out.print("--->");
        key = scanner.nextInt();
        order.getBouquets().remove(key - 1);
        System.out.println();
    }

    private void showOrder() {
        if (order.getBouquets().size() == 0) {
            Application.logger.trace("Your order is empty");
        } else {
            order.getBouquets().forEach(System.out::println);
        }
    }

    private void decorate() {
        Application.logger.trace("Input number of the bouquet in order of addition\n");
        int key;
        System.out.print("--->");
        key = scanner.nextInt();
        Bouquet decorated = order.getBouquets().get(key - 1);
        System.out.println();
        Application.logger.trace("~~~ Choose flowers that you want to add ~~~ ");
        BouquetDecorator tmp = new BouquetDecorator();
        flowers.values().forEach(System.out::println);
        System.out.print("--->");
        int flowers = scanner.nextInt();
        Application.logger.trace("Amount:");
        System.out.print("--->");
        int amount = scanner.nextInt();
        switch (flowers) {
            case 1:
                tmp = new PionuFlower(amount);
                break;
            case 2:
                tmp = new RomashkaFlower(amount);
                break;
            case 3:
                tmp = new RoseFlower(amount);
                break;
            case 4:
                tmp = new TulipFlower(amount);
                break;
            default:
                Application.logger.trace("Wrong flowers input".toUpperCase());
                break;
        }
        tmp.setFlowers(decorated);
        bouquetDecorator = tmp;
        order.getBouquets().set(key - 1, bouquetDecorator);
    }

    private void completeOrder() {
        Application.logger.trace("\n ~~~ Choose delivery type ~~~");
        deliveries.values().forEach(System.out::println);
        int deliveryKey;
        System.out.println("--->");
        deliveryKey = scanner.nextInt();
        order.setDelivery(Delivery.values()[deliveryKey - 1]);

        Application.logger.trace("\n ~~~ Choose packaging type ~~~");
        packages.values().forEach(System.out::println);
        int packagingKey;
        System.out.println("--->");
        packagingKey = scanner.nextInt();
        order.setPackaging(Package.values()[packagingKey - 1]);

        Application.logger.trace("\nYour final order: ");
        Application.logger.trace(order.getInfo());
        Application.logger.trace("\nThank you. We will contact you via email concerning payment. Bye!");
        System.exit(0);
    }

    private void chooseCity() {
        Application.logger.trace("~~~ Choose your city before the order ~~~");
        cities.values().forEach(System.out::println);
        int key;
        Application.logger.trace("\nInput your city");
        System.out.print("--->");
        key = scanner.nextInt();
        city = City.values()[key - 1];
        System.out.println();
    }

    private void chooseOccasion() {
        Application.logger.trace("\n~~~ Choose purpose for buying (price depends on it) ~~~");
        occasions.values().forEach(System.out::println);
        int key;
        Application.logger.trace("\nInput your purpose");
        System.out.print("--->");
        key = scanner.nextInt();
        occasion = Occasion.values()[key - 1];
        System.out.println();
    }

    private void chooseCard() {
        Application.logger.trace("\n~~~ Choose your client card ~~~");
        cards.values().forEach(System.out::println);
        int key;
        Application.logger.trace("\nInput your card");
        System.out.print("--->");
        key = scanner.nextInt();
        cardType = CardType.values()[key - 1];
        System.out.println();
    }

    private void showMenu() {
        Application.logger.trace("\n~~~~~~~~~~~~~ MENU ~~~~~~~~~~~~~");
        options.values().forEach(System.out::println);
        Application.logger.trace("~~~~~~~~~~~~~ MENU ~~~~~~~~~~~~~\n");
    }

    public void interact() {
        int mainKey;
        do {
            showMenu();
            Application.logger.trace("Chosen option");
            System.out.print("--->");
            mainKey = scanner.nextInt();
            methods.get(mainKey).execute();
        } while (mainKey != 6);
    }

    private void exit() {
        Application.logger.trace("Bye!");
        System.exit(0);
    }
}
