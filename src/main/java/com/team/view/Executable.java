package com.team.view;

@FunctionalInterface
public interface Executable {
    void execute();
}
